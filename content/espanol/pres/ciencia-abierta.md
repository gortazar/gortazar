---
title: Ciencia Abierta
date: 2022-05-26
feature: ciencia-abierta.png
teaser: ciencia-abierta.png

transpas:
    pdf: Ciencia_Abierta.pdf
    odp: Ciencia_Abierta.odp

type: pres
---

Presentación sobre ciencia abierta.
