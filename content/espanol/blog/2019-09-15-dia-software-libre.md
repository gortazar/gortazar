---
bg_image: /images/screenshot-from-2021-07-03-19-54-09.png
draft: false
author: OfiLibre
date: 2019-09-15
slug: dia-software-libre
thumb: images/blog/software-freedom/sfd.png
feature: images/blog/anuncio-web/logo-ofilibre-blanco.jpg
title: Día del Software Libre
description: Serie de charlas informativas sobre distintos tipos de software y
  alternativas con motivo del Día del Software Libre
type: post
categories:
  - eventos
tags:
  - charlas
---
El Día del Software Libre es una celebración anual internacional del Software Libre. Este día es un esfuerzo educativo público con el objetivo de aumentar el conocimiento del Software Libre y sus ventajas, y fomentar su uso. La Oficina de Conocimiento y Cultura Libres quiere celebrar este día con toda la comunidad universitaria, para ello, estamos organizando una serie de charlas sobre el Software Libre que tendrán lugar en el Campus de Móstoles el próximo 23 de septiembre de
13:00 a 15:00:

* "FPGAs libres: compartiendo hardware", Juan González Gómez
* "OpenVidu, una plataforma open source para añadir videoconferencia a tu aplicación web y móvil", Pablo Fuente Pérez
* "Software libre en robótica: ROS y URJC-JdeRobot", José María Cañas Plaza
* "Enriqueciendo el ecosistema de herramientas de código abierto para el desarrollo de pruebas web con Selenium", Boni García Gutiérrez
* "Damegender", David Arroyo Menéndez
* "Laboratorios de Linux en la Universidad Rey Juan Carlos: un caso de uso", Antonio Gutiérrez Mayoral
* "El emulador de redes NetGUI/Netkit para aprendizaje activo en asignaturas de Ingeniería Telemática", José Centeno González

**[Más información e inscripciones](https://eventos.urjc.es/39175/detail/dia-del-software-libre.html)**