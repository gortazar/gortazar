---
title: Una prueba más con imágenes en el cuerpo
date: 2023-04-13T10:04:33.643Z
description: Una desc cortita
slug: prueba-imagenes-2
type: post
categories:
  - OfiLibre
bg_image: /gortazar/images/Cuadrado_TFG_en_abierto.jpeg
thumb: /gortazar/images/logo-ofilibre.png
---
Vamos a pobner un texto e incrustar una imagen.

Por ejemplo aqui:

![Foto de cabecero con espejos](/gortazar/images/88c7b44a6d2acf358a45c7d84a4210aa.jpg "Foto de cabecero con espejos")