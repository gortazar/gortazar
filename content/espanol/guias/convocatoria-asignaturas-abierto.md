---
title: Reconocimiento de publicación de asignaturas en abierto
logo: logo-urjc-square.png
date: 2022-07-05
published: true
type: guias
---

[Preguntas más frecuentes sobre esta convocatoria](#faq)

Esta convocatoria se realiza en la Universidad Rey Juan Carlos para promover entre los docentes la creación de recursos educativos abiertos de calidad, y fomentar su uso en las asignaturas que dichos docentes imparten. La convocatoria es iniciativa de los Vicerrectorados de Transformación Digital e Innovación Docente, Extensión Universitaria y Ordenación Académica y Formación del Profesorado. Se enmarca dentro las acciones del proyecto colaborativo RED (Recursos educativos digitales: calidad y compartición en abierto), financiado en el marco del Plan UniDigital del Ministerio de Universidades.

Los detalles de la convocatoria pueden consultarse en el documento [Convocatoria
para el reconocimiento de publicación de asignaturas en acceso abierto 2022-2023](/documentos/convocatoria-asignaturas-abierto-2022-2023.pdf). A continuación se resumen y se explican algunos de sus aspectos más importantes. En caso de diferencia entre este resumen y el documento, tendrá validad el documento, que es el oficialmente publicado por la Universidad.

Los detalles sobre esta convocatoria se presentaron en sesión realizada el miércoles 13 de julio de 2022:

* Transparencias utilizadas en la presentación: [formato PDF](/transpas/convocatoria-asignaturas-abierto/Convocatoria_Asiganturas_Abierto.pdf), [formato ODF para LibreOffice](/transpas/convocatoria-asignaturas-abierto/Convocatoria_Asiganturas_Abierto.odp).
 * [Video de la presentación](https://tv.urjc.es/video/62e2b32867a0a661cb0b6e20).

En el siguiente horario habrá sesiones telemáticas públicas de resolución de dudas sobre la convocatoria, vía [sala de Teams](https://teams.microsoft.com/l/meetup-join/19%3ameeting_OWNhZjFhOTEtZmRjNi00YzQzLWI5NTQtZWU0Nzg3MmU0YWQ0%40thread.v2/0?context=%7b%22Tid%22%3a%225f84c4ea-370d-4b9e-830c-756f8bf1b51f%22%2c%22Oid%22%3a%22f39a6111-b3eb-43a6-98c0-a4d0f78c6742%22%7d):

* Martes 6 de septiembre, 17:00
* Miércoles 14 de septiembre, 12:00
* Martes 20 de septiembre, 17:00
* Miércoles 28 de septiembre, 12:00

### Objeto

Esta convocatoria pretende promover el trabajo del personal docente de la URJC para que publiquen sus materiales en asignaturas en acceso abierto, evaluándose dicho trabajo y asignando, en su caso, un incentivo económico y otros beneficios y efectos, por los que se reconoce el esfuerzo realizado en la elaboración de materiales publicados durante el curso 2022-23.

### Participación

La participación en esta convocatoria se hace proponiendo asignaturas del [aula virtual de la URJC](https://aulavirtual.urjc.es), que tendrán que ser de docencia oficial en un grado o máster universitario de la URJC durante el curso 2022-2023. Los materiales docentes de esas asignaturas se habrán publicado previamente en abierto.

### Publicación de materiales en acceso abierto

Los materiales que se sometan a evaluación deben estar tener en lugar visible la licencia bajo la que se publican, que tendrá que ser una de las licencias de publicación en acceso abierto aprobadas por el Consejo de Publicación Abierta de la URJC, con el consentimiento de todos sus autores (licencias [Atribución](https://creativecommons.org/licenses/by/4.0/deed.es) o [Atribución-CompartirIgual](https://creativecommons.org/licenses/by-sa/4.0/deed.es) de Creative Commons).

Los materiales en formato bibliográfico (guías, apuntes, colecciones de problemas y ejercicios, colecciones de exámenes, presentaciones, etc.) deben publicarse en el repositorio abierto institucional, [BURJC Digital](https://burjcdigital.urjc.es). Los vídeos y audios deben subirse a [TV URJC](https://tv.urjc.es/).

Una vez publicados los materiales, deberán enlazarse desde la asignatura correspondiente del aula virtual, en lugar claramente visible de forma que se pueda verificar fácilmente que los materiales publicados en abierto están referenciados en la asignatura. 

Puede consultarse el documento [Cómo publicar materiales docentes en abierto](/guias/materiales-docentes-abierto/) para entender el proceso completo de publicación en abierto de materiales docentes. En el caso específico de audios y videos, debe consultarse también el [Procedimiento de publicación de materiales en abierto en TV URJC](https://infotic.urjc.es/pages/viewpage.action?pageId=154370093).

### Presentación de solicitudes

Para propuesta de una asignatura, una vez sus materiales se hayan publicado en abierto, la realizará su responsable de grupo de actas (según figure en el Plan de Ordenación Docente) rellenando el [formulario de la convocatoria](https://forms.office.com/r/APNK3BMBNY). 

Las asignaturas que se impartan durante el primer cuatrimestre podrán presentarse hasta el día 30 de septiembre de 2022, y las que se impartan durante el segundo cuatrimestre, hasta el 20 de enero de 2023.

### Resolución de dudas

Para resolver dudas relacionadas con esta convocatoria, se pueden consultar las preguntas más frecuents, con sus respuestas, más adelante en este documento. En caso de que no se puedan resolver así, se podrán plantear por correo electrónico a la dirección ofilibre@urjc.es, donde se les tratará de dar solución lo antes posible.

### Preguntas frecuentes, con sus respuestas {#faq}

Para facilitar su consulta, todas aquellas dudas que se vayan resolviendo se incluirán en esta sección.


#### ¿Hay algún formato específico para los documentos (presentaciones, documentos de texto...)?

Aunque no es obligatorio, en la [OfiLibre](https://ofilibre.urjc.es) se han elaborado unas [plantillas que pueden ser uilizadas para los materiales publicados en acceso abierto](https://ofilibre.urjc.es/guias/plantillas-asignaturas-abierto/).


#### ¿Qué licencias son válidas?

Son dos las licencias que han sido aprobadas por el Consejo de Publicación Abierta de la URJC como licencias válidas para materiales en abierto:

* [Creative Commons BY 4.0](https://creativecommons.org/licenses/by/4.0/deed.es)

* [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es).

En la [Guía sobre la publicación de materiales docentes en acceso abierto](/guias/materiales-docentes-abierto/) se explican en más detalle ambas licencias. 


#### ¿Cómo especifico la licencia en mis documentos?

Dependiendo del tipo de documento se suele incluir la licencia o bien al principio, o al final. En documentos de texto, suele aparecer al principio donde normalmente iría la nota de copyright. En vídeos, puede aperecer al principio, donde aparezca el título, o al final donde aparecerían los títulos de crédito. En locuciones de audio, igualmente al principio o al final. Lo importante es que aparezca dentro del material de forma que se pueda reconocer la licencia. Puede ver más información en [esta guía de publicación de materiales dodentes en abierto]({{< ref "/guias/materiales-docentes-abierto#marcado-con-la-licencia-elegida" >}}) donde se explica con más detalle cómo incluir la licencia en los materiales.


#### ¿Quién puede subir los materiales de una asignatura al archivo abierto?

Los materiales puede subirlos cualquiera de los autores, previa firma y envío de un documento de autorización de publicación en abierto firmado por el autor que sube los materiales que sirve como declaración de conformidad.
        

#### Una vez que los materiales tienen la licencia, ¿cómo se suben al archivo abierto?

Las instrucciones para subir los materiales al archivo abierto institucional pueden encontrarse [en esta página de la BURJC](https://burjcdigital.urjc.es/page/howtopublish).

  
#### ¿Quién puede presentar la solicitud para mi asignatura en la convocatoria?

El responsable de la asignatura en el curso académico 2022-2023 es quien debe realizar la solicitud.


#### ¿Qué ocurre si mis materiales no encajan en ninguna de las categorías contempladas en la convocatoria?

En caso de materiales que no encajan en ninguna categoría, inclúyalos en otros y justifiquelo adecuadamente. 


#### ¿Dónde pongo mis recursos abiertos en mi asignatura de aula virtual?

De cara a la convocatoria, ponga los enlaces a los recursos abiertos en un sitio fácilmente accesible, como al principio de la página de contenidos, con independencia de que luego sean enlazados desde otras partes de la asignatura. Esto facilitará el acceso a los mismos durante el proceso de evaluación de solicitudes. Tenga en cuenta que los evaluadores deben encontrar fácilmente los enlaces a estos materiales.

#### ¿Cómo subo mis vídeos y los enlazo desde Aula Virtual?

Los vídeos y podcasts se deben subir primero a la plataforma [TV URJC](https://tv.urjc.es) y después enlazarlos para que queden visibles en la asignatura de Aula Virtual. Conviene que consultes el [Procedimiento de publicación de materiales en abierto en TV URJC](https://infotic.urjc.es/pages/viewpage.action?pageId=154370093). En este procedimiento se indica cómo los vídeos se deben publicar en TV URJC en una misma colección, que será la que se indique posteriormente en el formulario de solicitud de esta convocatoria. También el procedimiento muestra cómo insertar en Aula Virtual los vídeos subidos a TV URJC.

Los vídeos subidos desde TV URJC a Aula Virtual siguiendo este procedimiento son visibles para los estudiantes de la asignatura. Si la asignatura se aprueba para que esté disponible en abierto, esa colección se abrirá al público en general, por lo que la podrá ver todo el mundo.


#### Ya he enviado el formulario, pero quiero realizar cambios, ¿cómo lo hago?

Una vez enviado el formulario, no se puede modificar. Puede rellenar el formulario de nuevo, y el comité evaluador considerará siempre la última versión enviada para cada asignatura.

#### ¿Puedo aportar software (programas de ordenador) que utilizo en mis clases en alguna categoría?

El software libre que se utiliza en docencia podría aportarse en la categoría "Otros materiales" de la convocatoria, si cumple ciertos requisitos. Para explicar cuáles son estos requisitos, y en general para su consulta por los docentes que son autores de software libre que utilizan en sus clases hemos preparado unas recomendaciones específicas: [Convocatoria de asignaturas en abierto: software para docencia"](/guias/convocatoria-asignaturas-abierto-software)

#### ¿Cómo se vería mi asignatura en abierto? ¿Puedo ver algún ejemplo?

Para ver una asignatura en abierto, puedes ir al Aula Virtual y antes de ingresar, hay varias disponibles. [Ejemplo 1](https://www.aulavirtual.urjc.es/moodle/course/view.php?id=171109)
[Ejemplo 2](https://www.aulavirtual.urjc.es/moodle/theme/urjc/code/courses.php)

#### ¿Tengo que cambiar la forma de presentar mis materiales para participar?

No necesariamente. Aunque para subirlos al Archivo Abierto Institucional de la URJC (BURJC Digital) debe estar agrupados. La idea es mantener la forma en que damos las clases y hacer un compilado para depositar.
